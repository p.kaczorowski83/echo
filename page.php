<?php
/**
 * ===============================
 * PAGE.PHP - The template for displaying default theme page
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
get_header();
$content = get_field('editor_cnt');
?>

<main class="main main-default">
    <div class="container">
        <?php echo $content;?>
    </div><!-- edn /.container -->
</main>


<?php
get_footer();

