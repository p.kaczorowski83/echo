<?php
/**
 * ===============================
 * INDEX.PHP - The template for displaying content in the home page
 * Template Name: Strona główna
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main" id="scroll">

	<?php 
	get_template_part('template-parts/partial', 'strengths');
	get_template_part('template-parts/partial', 'box-line');
	get_template_part('template-parts/partial', 'case-study-slider');
	get_template_part('template-parts/partial', 'box-50-50-black');
	?>

</main>

<?php
get_footer();
