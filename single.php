<?php
/**
 * ===============================
 * SINGLE WORK .PHP - display single post
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
  
  get_header();
?>

<main class="main">

	<?php 
	get_template_part( 'template-parts/partial', 'single-post' );
	get_template_part( 'template-parts/partial', 'single-recommended' );
	?>

</main>

<?php
get_footer();