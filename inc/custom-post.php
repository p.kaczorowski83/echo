<?php
// Register Custom Post Type
function custom_post_case() {

  $labels = array(
    'name'                  => _x( 'Case Study', 'Post Type General Name', 'fastlogic' ),
    'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'fastlogic' ),
    'menu_name'             => __( 'Case Study', 'fastlogic' ),
    'name_admin_bar'        => __( 'Post Type', 'fastlogic' ),
  );
  $args = array(
    'label'                 => __( 'Case Study', 'fastlogic' ),
    'description'           => __( 'Post Type Description', 'fastlogic' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor' ),
    'taxonomies'            => array( 'cat-case-study' ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'           => 'dashicons-thumbs-up',
  );
  register_post_type( 'case-study', $args );

}
add_action( 'init', 'custom_post_case', 0 );

// Register Custom Taxonomy
function custom_case() {

  $labels = array(
    'name'                       => _x( 'Case Study -', 'Taxonomy General Name', 'fastlogic' ),
    'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'fastlogic' ),
    'menu_name'                  => __( 'Kategorie', 'fastlogic' ),
    'all_items'                  => __( 'Wszystkie case', 'fastlogic' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'cat-case-study', array( 'case-study' ), $args );

}
add_action( 'init', 'custom_case', 0 );


// Register Custom Post Type
function custom_post_jobs() {

  $labels = array(
    'name'                  => _x( 'Praca', 'Post Type General Name', 'fastlogic' ),
    'singular_name'         => _x( 'Praca', 'Post Type Singular Name', 'fastlogic' ),
    'menu_name'             => __( 'Praca', 'fastlogic' ),
    'name_admin_bar'        => __( 'Post Type', 'fastlogic' ),
  );
  $args = array(
    'label'                 => __( 'Case Study', 'fastlogic' ),
    'description'           => __( 'Post Type Description', 'fastlogic' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor' ),
    'taxonomies'            => array( 'kat-oferty-pracy' ),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'           => 'dashicons-admin-users',
  );
  register_post_type( 'oferty-pracy', $args );

}
add_action( 'init', 'custom_post_jobs', 0 );

// Register Custom Taxonomy
function custom_jobs() {

  $labels = array(
    'name'                       => _x( 'Oferty pracy -', 'Taxonomy General Name', 'fastlogic' ),
    'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'fastlogic' ),
    'menu_name'                  => __( 'Kategorie', 'fastlogic' ),
    'all_items'                  => __( 'Wszystkie ogłoszenia', 'fastlogic' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'kat-oferty-pracy', array( 'oferty-pracy' ), $args );

}
add_action( 'init', 'custom_jobs', 0 );



function custom_post_uslugi() {

  $labels = array(
    'name'                  => _x( 'Usługi', 'Post Type General Name', 'fastlogic' ),
    'singular_name'         => _x( 'Usługi', 'Post Type Singular Name', 'fastlogic' ),
    'menu_name'             => __( 'Usługi', 'fastlogic'),
    'name_admin_bar'        => __( 'Post Type', 'fastlogic'),
  );
  $args = array(
    'label'                 => __( 'Usługi', 'fastlogic' ),
    'description'           => __( 'Post Type Description', 'fastlogic' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor' ),
    'taxonomies'            => array( 'kat-uslugi'),
    'hierarchical'          => true,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'           => 'dashicons-admin-tools',
  );
  register_post_type( 'uslugi', $args );

}
add_action( 'init', 'custom_post_uslugi', 0 );

// Register Custom Taxonomy
function custom_uslugi() {

  $labels = array(
    'name'                       => _x( 'Usługi -', 'Taxonomy General Name', 'fastlogic' ),
    'singular_name'              => _x( 'Kategorie', 'Taxonomy Singular Name', 'fastlogic' ),
    'menu_name'                  => __( 'Kategorie', 'fastlogic' ),
    'all_items'                  => __( 'Wszystkie case', 'fastlogic' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'kat-uslugi', array( 'uslugi' ), $args );
  }
add_action( 'init', 'custom_uslugi', 0 );