<?php
/**
 * ===============================
 * SINGLE WORK .PHP - display single work post
 * ===============================
 *
 * @package CBK
 * @since 1.0.0
 * @version 1.0.0
 */
  
  get_header();

  $work_single_lead = get_field('work_single_lead');
  $work_single_details_persona = get_post_meta(get_the_ID(), 'work_single_details_persona', true );
  $work_single_details_location = get_post_meta(get_the_ID(), 'work_single_details_location', true );
  $work_single_details_experience = get_post_meta(get_the_ID(), 'work_single_details_experience', true );
  $work_single_details_agreement = get_post_meta(get_the_ID(), 'work_single_details_agreement', true );

  $work_single_cnt = get_field('work_single_cnt');
?>

<main class="main">

	<div class="container">
		
		<section class="career__single">
		
		<div class="career__single-lead">
			<?php echo $work_single_lead;?>
		</div>

		<div class="career__single-details">
			<ul>
				<?php if ($work_single_details_persona): ?>
				<li>
					<figure>
						<img loading="lazy" class="lazyload" data-src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-career-1.svg" alt="" />
					</figure>
					<span><?php echo _e( 'Rodzaj pracownika:', 'fastlogic' ) ?></span>
					<strong><?php esc_html_e( $work_single_details_persona , 'fastlogic' ); ?></strong>
				</li>
				<?php endif ?>
				<?php if ($work_single_details_location): ?>
				<li>
					<figure>
						<img loading="lazy" class="lazyload" data-src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-career-2.svg" alt="" />
					</figure>
					<span><?php echo _e( 'Lokalizacja:', 'fastlogic' ) ?></span>
					<strong><?php esc_html_e( $work_single_details_location, 'fastlogic' ); ?></strong>
				</li>
				<?php endif ?>
				<?php if ($work_single_details_experience): ?>
				<li>
					<figure>
						<img loading="lazy" class="lazyload" data-src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-career-3.svg" alt="" />
					</figure>
					<span><?php echo _e( 'Doświadczenie:', 'fastlogic' ) ?></span>
					<strong><?php esc_html_e( $work_single_details_experience, 'fastlogic' ); ?></strong>
				</li>
				<?php endif ?>
				<?php if ($work_single_details_agreement): ?>
				<li>
					<figure>
						<img loading="lazy" class="lazyload" data-src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-career-4.svg" alt="" />
					</figure>
					<span><?php echo _e( 'Rodzaj umowy:', 'fastlogic' ) ?></span>
					<strong><?php esc_html_e( $work_single_details_agreement, 'fastlogic' ); ?></strong>
				</li>
				<?php endif ?>
			</ul>
		</div>

		<?php if ($work_single_cnt): ?>
			<div class="career__single-cnt">
				<?php echo $work_single_cnt;?>
			</div>			
		<?php endif ?>

		<div class="text-center">
			<a href="mailto:office@fastlogic.pl">Aplikuj poprzez e-mail</a>
		</div>

	</section>
	</div>

</main>

<?php
get_footer();