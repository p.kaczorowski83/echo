<?php
/**
 * ===============================
 * INDEX.PHP - The template for displaying content case study
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

get_header();
?>

<main class="main" id="scroll">

	<?php 
	get_template_part('template-parts/partial', 'case-study-category');
	get_template_part('template-parts/partial', 'case-study-list');
	?>

</main>

<?php
get_footer();
