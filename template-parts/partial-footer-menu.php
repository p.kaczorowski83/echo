<?php
/**
 * ===============================
 * FOOTER MENU .PHP - footer menu section
 * ===============================
 *
 * @package fastlogic
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<nav class="footer__logo-menu">
	<div class="container">
	<?php
	wp_nav_menu(
		array(
		'theme_location' => 'main-menu',
		'container'      => '',
		'menu_class'     => 'footer-nav',
		'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
		'walker'         => new WP_Bootstrap_Navwalker(),
		)
	);
	?>	
	</div>	
</nav>
