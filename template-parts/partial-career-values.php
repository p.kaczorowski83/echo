<?php
/**
 * ===============================
 * PARTIAL CAREER VALUES.PHP - career-values
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
$career_values_title = get_field( 'career_values_title' );

$allowed_types = array(
	'span'      => array(),
);

?>

<section class="career__values">
	<div class="container">

		<?php if ($career_values_title): ?>			
			<h2><?php echo wp_kses( __( $career_values_title, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php if ( have_rows( 'career_values' ) ) : ?>
			<ul class="career__loop">
				<?php while ( have_rows( 'career_values' ) ) : the_row(); ?>
					<li>
						<?php $career_values_img = get_sub_field( 'career_values_img' ); ?>
						<?php $size = 'full'; ?>
						<?php if ( $career_values_img ) : ?>
							<?php echo wp_get_attachment_image( $career_values_img, $size, false, [
								    'class' => 'lazyload img-fluid',
								    'loading' => 'lazy',
								    'data-src' => wp_get_attachment_image_url( $career_values_img, $size )
								]); ?>
						<?php endif; ?>
						<h4><?php the_sub_field( 'career_values_txt' ); ?></h4>
					</li>
				<?php endwhile; ?>
			</ul>
		<?php endif; ?>

		
	</div>

</section>