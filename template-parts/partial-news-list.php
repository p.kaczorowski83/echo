<?php
/**
 * ===============================
 * PARTIAL NEWS LIST.PHP - news list 
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */


?>

<section>
	<div class="container">
		<ul class="news__list">
	    <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
        'posts_per_page' => 5,
        'paged' => $paged,
	    );
	    $loop = new WP_Query( $args );
	    if ( $loop->have_posts() ) {
	    while ( $loop->have_posts() ) : $loop->the_post();
	    $news_lead = get_post_meta(get_the_ID(), 'news_lead', true );
	    ?>
	    <li>
	    	<a href="<?php the_permalink();?>" title="<?php the_title();?>">
	    		<span><?php the_time('j, F, Y');?></span>
		    	<h3><?php the_title();?></h3>
		    	<p><?php echo wp_kses( __($news_lead, 'fastlogic' ), $allowed_types ); ?></p>
		    	<?php $news_img = get_field( 'news_img' ); ?>
				<?php $size = 'imageNews'; ?>
				<?php if ( $news_img ) : ?>
					<?php echo wp_get_attachment_image( $news_img, $size, false, [
					    'class' => 'lazyload img-fluid',
					    'loading' => 'lazy',
					    'data-src' => wp_get_attachment_image_url( $news_img, $size )
					]); ?>
				<?php endif; ?>
	    	</a>
	    </li>
	    <?php endwhile;?>
		</ul>

		<?php
	    $total_pages = $loop->max_num_pages;
	    if ($total_pages > 1){ ?>

	    <div class="paginate__links">
	        <?php 
	        $current_page = max(1, get_query_var('paged'));

	        echo paginate_links(array(
	            'base' => get_pagenum_link(1) . '%_%',
	            'format' => '/page/%#%',
	            'current' => $current_page,
	            'total' => $total_pages,
	            'prev_text'    => __('<svg width="8" height="14" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 18L2 10L10 2" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/></svg> poprzednia'),
	            'next_text' => __('następna <svg width="8" height="14" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 18L10 10L2 2" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/></svg>'),
	        ));
	        ?>
	    </div>
	    <?php }
	    }
	    wp_reset_postdata();
	    ?>  
	</div>

</section>

