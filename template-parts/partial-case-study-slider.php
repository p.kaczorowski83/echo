<?php
/**
 * ===============================
 * CASE STUDY SLIDER.PHP - show slider with select case study
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$case_study_slider_title = get_post_meta(get_the_ID(), 'case_study_slider_title', true );

$allowed_types = array(
	'span'      => array(),
);
?>

<div class="case__study-slider">
	<div class="container">
	<?php if ($case_study_slider_title): ?>			
		<h2><?php echo wp_kses( __( $case_study_slider_title, 'fastlogic' ), $allowed_types ); ?></h2>
	<?php endif ?>
	</div>

		<?php $case_study_slider = get_field( 'case_study_slider' ); ?>
		<?php if ( $case_study_slider ) : ?>
			<div class="owl-carousel owl-theme">
			<?php foreach ( $case_study_slider as $post ) : ?>
				<?php setup_postdata ( $post ); ?>
				<div class="item">
					<a href="<?php the_permalink(); ?>">
						<?php $case_img = get_field( 'case_img',$post->ID ); ?>
						<?php $size = 'image1030'; ?>
						<?php if ( $case_img ) : ?>
							<?php echo wp_get_attachment_image( $case_img, $size, false, [
							    'class' => 'lazyload',
							    'loading' => 'lazy',
							    'data-src' => wp_get_attachment_image_url( $case_img, $size )
							]); ?>
						<?php endif; ?>
						<div class="casestudy__list-apla"></div>
						<div class="casestudy__list-cnt">
						<span><?php $terms = get_the_terms($post->ID, 'cat-case-study' );foreach ($terms as $term) {echo "".$term_name = $term->name.'';}?></span>
						<h3><?php the_title();?></h3>
					</div>	
					</a>
				</div>
			<?php endforeach; ?>
			<?php wp_reset_postdata(); ?>
		</div>
		
		<div id="counter"></div>
		
		<?php endif; ?>
</div>