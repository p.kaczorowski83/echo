<?php
/**
 * ===============================
 * PARTIAL CAREER LIST .PHP - career list work
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
$career_work = get_field( 'career_work' );
$career_work_lead = get_field( 'career_work_lead' );

$allowed_types = array(
	'span'      => array(),
	'br'      => array(),
);

?>

<section class="career__work">
	<div class="container">

		<div class="career__work-lead">	
			<?php if ($career_work): ?>			
				<h2><?php echo wp_kses( __( $career_work, 'fastlogic' ), $allowed_types ); ?></h2>
			<?php endif ?>

			<?php if ($career_work_lead): ?>
				<p><?php echo wp_kses( __( $career_work_lead, 'fastlogic' ), $allowed_types ); ?></p>
			<?php endif ?>
		</div>	

		<ul class="career__work-list">
			<?php $career_work_post = get_field( 'career_work_post' ); ?>
			<?php if ( $career_work_post ) : ?>
				<?php foreach ( $career_work_post as $post ) : ?>
					<?php setup_postdata ( $post ); ?>
					<li>
						<a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
							<div class="carrer__work-text">
								<?php the_title();?>
								<span><?php echo _e( 'Miejsce pracy', 'fastlogic' ) ?>: <strong><?php the_field( 'work_single_details_location' ); ?></strong></span>
							</div>			
							<div class="career__work-apply">
								<span>
									<?php echo _e( 'Aplikuj', 'fastlogic' ) ?>
								</span>
							</div>
						</a>
					</li>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</ul>
		
	</div>
</section>