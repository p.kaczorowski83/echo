<?php
/**
 * ===============================
 * PARTIAL CONTACT MAP .PHP - show section with map
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
$contact_form = get_field( 'contact_form' );

$allowed_types = array(
	'span'      => array(),
);
?>

<section class="contact__form">
	<div class="container">
		<?php if ($contact_form): ?>			
			<h2><?php echo wp_kses( __( $contact_form, 'fastlogic' ), $allowed_types ); ?></h2>
		<?php endif ?>

		<?php echo do_shortcode('[contact-form-7 id="316" title="Kontakt"]');?>

	</div>	
</section>	