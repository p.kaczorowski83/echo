<?php
/**
 * ===============================
 * PARTIAL CONTACT MAP .PHP - show section with map
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$contact_cnt = get_field('contact_cnt');

?>

<div class="container">
	<section class="contact__row">
		<div class="contact__row-text">
			<?php echo $contact_cnt ?>
		</div>

		<div class="contact__row-map">
			<?php $contact_maps = get_field( 'contact_maps' ); ?>
			<?php $size = 'full'; ?>
				<?php echo wp_get_attachment_image( $contact_maps, $size, false, [
					'class' => 'lazyload img-fluid',
					'loading' => 'lazy',
					'data-src' => wp_get_attachment_image_url( $contact_maps, $size )
				]); ?>
		</div>
	</section>	
</div>	