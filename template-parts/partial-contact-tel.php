<?php
/**
 * ===============================
 * PARTIAL CONTACT TEL .PHP - show adres email adn contact number
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$footer_email = get_option('options_footer_email');
$footer_phone = get_option('options_footer_phone');
$phone = str_replace(' ', '', get_option("options_footer_phone"));

?>

<div class="contact__tel">
	<div class="container">
		<ul class="footer__logo-tel">
			<li><a href="tel:<?php echo $phone;?>"><?php esc_html_e( $footer_phone, 'fastlogic' ); ?></a></li>
			<li><a href="mailto:<?php esc_html_e( $footer_email, 'fastlogic' ); ?>"><?php esc_html_e( $footer_email, 'fastlogic' ); ?></a></li>
		</ul>
	</div>	
</div>	
