<?php
/**
 * ===============================
 * PARTIAL CASE STUDY LIST.PHP - case study list post
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
$career_benefits_title = get_field( 'career_benefits_title' );

$allowed_types = array(
	'span'      => array(),
);

?>

	<div class="container">

		<ul class="casestudy__list">
		    <?php
	        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	        $args = array(
	        'posts_per_page' => 4,
	        'paged' => $paged,
	        'post_type' => 'case-study',
		    );
		    $loop = new WP_Query( $args );
		    if ( $loop->have_posts() ) {
		    while ( $loop->have_posts() ) : $loop->the_post();
		    $categories = get_the_terms($post->ID, 'category');
		    ?>
		    <li>
		    	<a href="<?php the_permalink();?>" title="<?php the_title();?>">
		    		<?php $case_img = get_field( 'case_img' ); ?>
		    		<?php $size = 'image1250'; ?>
			    	<?php echo wp_get_attachment_image( $case_img, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $case_img, $size )
					]); ?>
					<div class="casestudy__list-apla"></div>
					<div class="casestudy__list-cnt">
						<span><?php $terms = get_the_terms($post->ID, 'cat-case-study' );foreach ($terms as $term) {echo "".$term_name = $term->name.'';}?></span>
						<h3><?php the_title();?></h3>
					</div>		    	
		    	</a>
		    </li>
		    <?php endwhile;?>
		</ul>

		<?php
	    $total_pages = $loop->max_num_pages;
	    if ($total_pages > 1){ ?>

	    <div class="paginate__links">
	        <?php 
	        $current_page = max(1, get_query_var('paged'));

	        echo paginate_links(array(
	            'base' => get_pagenum_link(1) . '%_%',
	            'format' => '/page/%#%',
	            'current' => $current_page,
	            'total' => $total_pages,
	            'prev_text'    => __('<svg width="8" height="14" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10 18L2 10L10 2" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/></svg> poprzednia'),
	            'next_text' => __('następna <svg width="8" height="14" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 18L10 10L2 2" stroke="black" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/></svg>'),
	        ));
	        ?>
	    </div>
	    <?php }
	    }
	    wp_reset_postdata();
	    ?>	
	</div>
