<?php
/**
 * ===============================
 * FOOTER COPY .PHP - footer ue section
 * ===============================
 *
 * @package fastlogic
 * @since 1.0.0
 * @version 1.0.0
 */

?>

<div class="footer__copy">	
	<div class="container">
		Copyright © <strong>Fastlogic sp. z o.o. <?php echo date('Y') ?></strong> <span>|</span> Created by <a href="https://qualitypixels.pl/projektowanie-stron-internetowych/" target="_blank">Quality Pixels</a> 	
	</div>
</div>