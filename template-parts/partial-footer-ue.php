<?php
/**
 * ===============================
 * FOOTER UE .PHP - footer ue section
 * ===============================
 *
 * @package fastlogic
 * @since 1.0.0
 * @version 1.0.0
 */
$footer_ue = get_option('options_footer_ue');
?>

<?php if ( get_field( 'footer_ue_switch', 'option' ) == 1 ) : ?>
	<?php if ($footer_ue): ?>
		<div class="footer__ue">
		    <div class="container">	
		    <?php $size = 'full';?>
			<?php if ($footer_ue ) : ?>
				<?php echo wp_get_attachment_image( $footer_ue, $size, false, [
				    'class' => 'lazyload footer-ue img-fluid',
				    'loading' => 'lazy',
				    'data-src' => wp_get_attachment_image_url( $footer_ue, $size )
				]); ?>
			<?php endif; ?>			
			</div>
		</div>
	<?php endif ?>
<?php endif ?>