<?php
/**
 * ===============================
 * HEADER SINGLE RECOMMENDED.PHP - single post recommended
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
  $news_cnt = get_field('news_cnt');
  $news_lead = get_post_meta(get_the_ID(), 'news_lead', true );
  $allowed_types = array(
	'span'      => array(),
   );


?>

<section class="news__single-recommended">
	<div class="container">	
		<h2><?php echo _e( 'Zobacz <span>także</span>', 'fastlogic'); ?></h2>
		<ul class="news__list">
	    <?php
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
        'posts_per_page' => 1,
        'paged' => $paged,
        'post__not_in' => array( $post->ID )
	    );
	    $loop = new WP_Query( $args );
	    if ( $loop->have_posts() ) {
	    while ( $loop->have_posts() ) : $loop->the_post();
	    $news_lead = get_post_meta(get_the_ID(), 'news_lead', true );
	    ?>
	    <li>
	    	<a href="<?php the_permalink();?>" title="<?php the_title();?>">
	    		<span><?php the_time('j, F, Y');?></span>
		    	<h3><?php the_title();?></h3>
		    	<p><?php echo wp_kses( __($news_lead, 'fastlogic' ), $allowed_types ); ?></p>
		    	<?php $news_img = get_field( 'news_img' ); ?>
				<?php $size = 'imageNews'; ?>
				<?php if ( $news_img ) : ?>
					<?php echo wp_get_attachment_image( $news_img, $size, false, [
					    'class' => 'lazyload img-fluid',
					    'loading' => 'lazy',
					    'data-src' => wp_get_attachment_image_url( $news_img, $size )
					]); ?>
				<?php endif; ?>
	    	</a>
	    </li>
	    <?php endwhile;?>
		</ul>		
	    <?php 
	    }
	    wp_reset_postdata();
	    ?>  
	</div>
</section>

