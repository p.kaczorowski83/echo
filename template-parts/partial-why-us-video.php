<?php
/**
 * ===============================
 * PARTIAL WHY US TEAM.PHP - why-us-leaders
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */
$whyus_video_file = get_field( 'whyus_video_file' );
$whyus_video_yt = get_field('whyus_video_yt');
?>

<div class="whyus">
	<div class="container">

		<?php $whyus_video = get_field( 'whyus_video' ); ?>
		<?php $size = 'full'; ?>
		<?php if ( $whyus_video ) : ?>
			<div class="whyus__video">
				<a href="<?php if ($whyus_video_file):?><?php echo esc_url($whyus_video_file['url']); ?><?php elseif($whyus_video_yt):?><?php echo $whyus_video_yt;?><?php endif;?>" data-fancybox="bigvideo" data-width="640" data-height="360">
					<?php echo wp_get_attachment_image( $whyus_video, $size, false, [
					'class' => 'lazyload img-fluid',
					'loading' => 'lazy',
					'data-src' => wp_get_attachment_image_url( $whyus_video, $size )
					]); ?>
				</a>
			</div>
		<?php endif; ?>

	</div>

</div>

