<?php
/**
 * ===============================
 * CASE STUDY SLIDER.PHP - show slider with select case study
 * ===============================
 *
 * @package FASTLOGIC
 * @since 1.0.0
 * @version 1.0.0
 */

$case_study_slider_title = get_post_meta(get_the_ID(), 'case_study_slider_title', true );

$allowed_types = array(
	'span'      => array(),
);
?>

<div class="case__study-slider-single">
	<div class="container">
	<h2><?php echo _e( 'Zobacz <span>także</span>', 'fastlogic'); ?></h2>
	</div>

	<div class="owl-carousel owl-theme">
		<?php
        $args = array(
       	'post_type' => 'case-study',
        'posts_per_page' => 10,
        'post__not_in' => array( $post->ID )
	    );
	    $loop = new WP_Query( $args );
	    if ( $loop->have_posts() ) {
	    while ( $loop->have_posts() ) : $loop->the_post();
	    ?>
				<div class="item">
					<a href="<?php the_permalink();?>" title="<?php the_title();?>">
		    		<?php $case_img = get_field( 'case_img' ); ?>
		    		<?php $size = 'image1030'; ?>
			    	<?php echo wp_get_attachment_image( $case_img, $size, false, [
						'class' => 'lazyload img-fluid',
						'loading' => 'lazy',
						'data-src' => wp_get_attachment_image_url( $case_img, $size )
					]); ?>
					<div class="casestudy__list-apla"></div>
					<div class="casestudy__list-cnt">
						<span><?php $terms = get_the_terms($post->ID, 'cat-case-study' );foreach ($terms as $term) {echo "".$term_name = $term->name.'';}?></span>
						<h3><?php the_title();?></h3>
					</div>		    	
		    	</a>
				</div>
			<?php endwhile;?>
			<?php }
	    wp_reset_postdata();
	    ?>	
		</div>

		<div id="counter"></div>
</div>